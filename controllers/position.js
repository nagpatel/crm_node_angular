const Position = require('../models/Position');

const errorHandler = require('../utils/errorHandler');

module.exports.getByCategoryId = async (req, resp, next) => {
    try {
        const positions = await Position.find({
            category: req.params.categoryId,
            user: req.user.id
        });
    } catch (error) {
        errorHandler(resp, error)
    }
};

module.exports.create = async (req, resp, next) => {
    try {
        const position = await new Position({
            name: req.body.name,
            cost: req.body.cost,
            category: req.body.category,
        }).save();

        resp.status(201).json(position);
    } catch (error) {
        errorHandler(resp, error)
    }
};

module.exports.update = async (req, resp, next) => {
    try {
        const position = await Position.findOneAndUpdate({
            _id: req.params.id
        }, {
            $set: req.body
        },
        {
            new: true
        });

        resp.status(200).json(position);
    } catch (error) {
        errorHandler(resp, error)
    }
};

module.exports.remove = async (req, resp, next) => {
    try {
        await Position.remove({
            _id: req.params.id
        });

        resp.status(200).json({ message: 'Позиция успешно удалена' });

    } catch (error) {
        errorHandler(resp, error)
    }
};