import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PositionsService} from "../../../shared/services/positions.service";
import {MaterialInstance, MaterialServcie} from "../../../shared/classes/material.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Position} from "../../../shared/interfaces";

@Component({
  selector: 'app-positions-form',
  templateUrl: './positions-form.component.html',
  styleUrls: ['./positions-form.component.scss']
})
export class PositionsFormComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input('categoryId') categoryId: string;

  @ViewChild('modal') modalRef: ElementRef;

  positions: Position[] = [];

  loading = false;

  modal: MaterialInstance;

  form: FormGroup;

  positionId = null;

  constructor(private positionsService: PositionsService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      cost: new FormControl(1, [Validators.required, Validators.min(1)]),
    });

    this.loading = true;

    this.positionsService.fetch(this.categoryId).subscribe(positions => {
      this.positions = positions;

      this.loading = false;
    }, error => {});
  }

  ngOnDestroy(): void {
    this.modal.destroy();
  }

  ngAfterViewInit(): void {
    this.modal = MaterialServcie.initModal(this.modalRef);
  }

  onSelectPosition(position: Position) {
    this.positionId = position._id;

    this.form.patchValue({
      name: position.name,
      cost: position.cost
    });

    this.modal.open();

    MaterialServcie.updateTextInputs();
  }

  onAddPosition(): void {
    this.positionId = null;

    this.form.reset({
      name: null,
      cost: 1
    });

    this.modal.open();

    MaterialServcie.updateTextInputs();
  }

  onCancel(): void {
    this.modal.close();
  }

  onSubmit() {
    this.form.disable();

    const newPosition: Position = {
      name: this.form.value.name,
      cost: this.form.value.cost,
      category: this.categoryId
    };

    const completed = () => {
      this.modal.close();

      this.form.reset({
        name: '',
        cost: 1
      });

      this.form.enable();
    };

    if (this.positionId) {
      newPosition._id = this.positionId;

      this.positionsService.update(newPosition).subscribe(position => {
          const idx = this.positions.findIndex(p => p._id === position._id);

          this.positions[idx] = position;

          MaterialServcie.toast('Изменения сохранены');
        }, error => {
          this.form.enable();

          MaterialServcie.toast(error.error.message);
        },
        completed);
    }
    else {
      this.positionsService.create(newPosition).subscribe(position => {
          MaterialServcie.toast('Позиция создана');

          this.positions.push(position);
        }, error => {
          this.form.enable();

          MaterialServcie.toast(error.error.message);
        },
        completed);
    }
  }

  onDeletePosition(event: Event, position) {
    event.stopPropagation();

    const decision = window.confirm(`Удалить позицию ${position.name}`);

    if (decision) {
      this.positionsService.delete(position).subscribe((response) => {
        const idx = this.positions.findIndex(p => p._id === position._id);

        this.positions.splice(idx, 1);

        MaterialServcie.toast(response.message);
      }, error => {
        MaterialServcie.toast(error.error.message);
      });
    }
  }
}
