import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CategoriesService} from "../../shared/services/categories.service";
import {switchMap} from "rxjs/operators";
import {of} from "rxjs";
import {MaterialServcie} from "../../shared/classes/material.service";
import {Category} from "../../shared/interfaces";
import {error} from "selenium-webdriver";

@Component({
  selector: 'app-categories-form',
  templateUrl: './categories-form.component.html',
  styleUrls: ['./categories-form.component.scss']
})
export class CategoriesFormComponent implements OnInit {
  @ViewChild('input') inputRef: ElementRef;
  isNew = false;

  image: File;

  form: FormGroup;

  imagePreview: string | ArrayBuffer = '';

  category: Category;

  constructor(private route: ActivatedRoute, private categoriesService: CategoriesService,
              private router: Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required)
    });

    this.form.disable();

    this.route.params.pipe(switchMap((params: Params) => {
      if (params) {
        this.isNew = false;

        this.categoriesService.getById(params['id']);
      }

      return of(null);
    }))
      .subscribe((category: Category) => {
        if (category) {
          this.category = category;

          this.form.patchValue({
            name: category.name
          });

          this.imagePreview = category.imgSrc;

          MaterialServcie.updateTextInputs();
        }

        this.form.enable();
      }, error => {
        MaterialServcie.toast(error.error.message);
      });
  }

  onSubmit() {
    let obs$;

    this.form.disable();

    if (this.isNew) {
      obs$ = this.categoriesService.create(this.form.value.name, this.image);
    }
    else {
      obs$ = this.categoriesService.update(this.category._id, this.form.value.name, this.image);
    }

    obs$.subscribe(
      category => {
        this.form.enable();

        MaterialServcie.toast('Изменения сохранены');

        this.category = category;
      },
      error => {
        this.form.enable();

        MaterialServcie.toast(error.error.message);

        console.log(error);
      }
    );
  }

  triggerClick() {
    this.inputRef.nativeElement.click();
  }

  onFileUpload(event: any) {
    const file = event.target.files[0];

    this.image = file;

    const reader = new FileReader();

    reader.onload = () => {
      this.imagePreview = reader.result;
    };

    reader.readAsDataURL(file);
  }

  deleteCategory() {
    const decision = `Вы уверены, что хотите удалить категорию ${this.category.name}`;

    if (decision) {
      this.categoriesService.delete(this.category._id).subscribe(response => MaterialServcie.toast(response.message),
        error => MaterialServcie.toast(error.error.message),
        () => this.router.navigate(['/categories']));
    }
  }

}
