import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {MaterialInstance, MaterialServcie} from "../shared/classes/material.service";
import {OrderService} from "./order.service";
import {OrdersService} from "../shared/services/orders.service";
import {Order, OrderPosition} from "../shared/interfaces";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.scss'],
  providers: [OrderService]
})
export class OrderPageComponent implements OnInit, OnDestroy, AfterViewInit, MaterialInstance {
  @ViewChild('modal') modalRef: ElementRef;

  isRoot: boolean;

  modal: MaterialInstance;

  pending = false;

  oSub: Subscription;

  constructor(private router: Router, public order: OrderService,
              private ordersService: OrdersService) {
  }

  ngOnInit(): void {
    this.isRoot = this.router.url === '/order';

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.isRoot = this.router.url === '/order';
      }
    }, error => {
    });
  }

  ngAfterViewInit(): void {
    this.modal = MaterialServcie.initModal(this.modalRef);
  }

  ngOnDestroy(): void {
    this.modal.destroy();

    if (this.oSub) {
      this.oSub.unsubscribe();
    }
  }

  open() {
    this.modal.open();
  }

  cancel() {
    this.modal.close();
  }

  submit() {
    this.pending = true;
    /**
     * we need to remove _id property, because we have to use in only on frontend part
     */
    const order: Order = {
      list: this.order.list.map(item => {
        delete item._id;

        return item;
      })
    };

    this.oSub = this.ordersService.create(order).subscribe((newOrder) => {
        MaterialServcie.toast(`Заказ №${newOrder.order} был добавлен.`);
      }, error => {
        MaterialServcie.toast(error.error.message);

        this.order.clear();
      },
      () => {
        this.modal.close();

        this.pending = false;
      });
  }

  removePosition(orderPosition: OrderPosition) {
    this.order.remove(orderPosition);
  }
}
